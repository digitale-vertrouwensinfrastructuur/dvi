import "./styles.css";

import React from "react";
import Credits from "../../components/Credits";
import { Canvas } from "react-three-fiber";

import Camera from "../../components/Camera2";
import Cube from "../../components/Cube";
import Lights from "../../components/Lights";
import Environment from "../../components/Environment";
import Controls from "../../components/Controls";
import Plane from "../../components/Plane";
import Text from "../../components/Text2";

export default () => {

  return (
    <>
      <Credits/>
      <Canvas>
        <Camera />
        <gridHelper args={[20, 40, "blue", "hotpink"]} />
        <axesHelper args={[2, 2, 2]} />
        <Controls />
        <Lights />

        <Environment />
        <Plane />
        {/* <fog attach="fog" args={["#041830", 5, 4]} /> */}
        
        {/* TOO SLOW!
        <React.Suspense fallback={null}>
          <Text hAlign="left" position={[16.5, -4.2, 0]} children="Meldkamer" />
        </React.Suspense> */}

        <Cube defaultColor={0x3f2fff} defaultPosition={[0,2,0]}/>
      </Canvas>
    </>
  );
};