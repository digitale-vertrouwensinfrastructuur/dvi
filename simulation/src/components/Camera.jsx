import React, { useRef, useEffect } from 'react'
import { useFrame, useThree } from 'react-three-fiber'

const distance = 500;

const rotationX = 0*Math.PI/180;
const rotationY = -45*Math.PI/180;
const rotationZ = 0//-45*Math.PI/180;

const initialCameraPositionY = -Math.tan(rotationX)*distance;
const initialCameraPositionX = Math.tan(rotationY)*Math.sqrt(distance**2 + initialCameraPositionY**2);
const initialCameraPositionZ = distance;

const Camera = (props) => {
  const ref = useRef()
  const { setDefaultCamera } = useThree()
  // Make the camera known to the system
  useEffect(() => void setDefaultCamera(ref.current), [setDefaultCamera])
  // Update it every frame
  useFrame(() => ref.current.updateMatrixWorld())
  return <orthographicCamera
    left={window.innerWidth/-2}
    right={window.innerWidth/2}
    top={window.innerHeight / 2}
    bottom={window.innerHeight / -2}
    near={0.1}
    far={10000}
    rotation={[rotationX, rotationY, rotationZ]}
    zoom={200}
    position={[initialCameraPositionX, initialCameraPositionY, initialCameraPositionZ]}
    ref={ref} {...props} />;
};

export default Camera;