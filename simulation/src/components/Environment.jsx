import React from "react";
import { BackSide } from "three";

export default () => {
  return (
    <mesh>
      <sphereBufferGeometry args={[20, 15, 15]} attach="geometry" />
      <meshStandardMaterial
        color={0x3fffff}
        attach="material"
        side={BackSide}
        metalness={0.4}
      />
    </mesh>
  );
};
