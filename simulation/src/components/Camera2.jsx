import React, { useRef, useEffect } from 'react'
import { useFrame, useThree } from 'react-three-fiber'

const distance = 500;

const rotationX = Math.atan( - 1 / Math.sqrt( 2 ) );
const rotationY = - Math.PI / 4;
const rotationZ = 0;

const initialCameraPositionY = 20;
const initialCameraPositionX = 20;
const initialCameraPositionZ = 20;

const Camera = (props) => {
  const ref = useRef()
  const { setDefaultCamera } = useThree()
  // Make the camera known to the system
  useEffect(() => void setDefaultCamera(ref.current), [setDefaultCamera])
  // Update it every frame
  useFrame(() => {
    ref.current.positionX += 1;
    //camera.position.x = cameraCenter.x + (cameraHorzLimit * mouse.x);
    //camera.position.y = cameraCenter.y + (cameraVertLimit * mouse.y);
    // ref.current.updateMatrixWorld()
  })
  return <orthographicCamera
    left={window.innerWidth/-2}
    right={window.innerWidth/2}
    top={window.innerHeight / 2}
    bottom={window.innerHeight / -2}
    near={0.1}
    far={10000}
    rotation={[rotationX, rotationY, rotationZ]}
    zoom={200}
    position={[initialCameraPositionX, initialCameraPositionY, initialCameraPositionZ]}
    ref={ref} {...props} />;
};

export default Camera;