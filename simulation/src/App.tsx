import React from 'react';
import './App.css';
import Viewer from "./views/Viewer";

function App() {
  return (
    <Viewer></Viewer>
  );
}

export default App;
